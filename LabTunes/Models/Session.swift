//
//  Session.swift
//  LabTunes
//
//  Created by Eric Martínez Paredes on 09/11/18.
//  Copyright © 2018 ACL. All rights reserved.
//

import Foundation

//Guardado de Token se sesión
class Session: NSObject {
    //Se crea unicamente cuando el login es válido
    var token: String?
    //Solamente crea una sola instancia de session
    static let sharedInstance = Session()
    
    //Evita que se pueda instanciar desde otrs clases o controladores
    override private init(){
        super.init()
    }
    
    func saveSession() {
        token = "1234567890"
    }
}
