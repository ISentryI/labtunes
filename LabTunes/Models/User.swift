//
//  User.swift
//  LabTunes
//
//  Created by Eric Martínez Paredes on 09/11/18.
//  Copyright © 2018 ACL. All rights reserved.
//

import Foundation

class User {
    static let userName = "username"
    static let password = "password"
    static let session = Session.sharedInstance
    
    static func login(userName: String, password: String) -> Bool {
        if self.userName == userName, self.password == password {
            session.saveSession()
            return true
        }
        else {
            return false
        }
    }
    
    static func fetchSongs() throws {
        guard let token = Session.sharedInstance.token else {
            throw UserError.notSessionAvailable
        }
        debugPrint(token)
        //XCTAssertThrowsError
    }
    
    enum UserError: Error {
        case notSessionAvailable
    }
}
