//
//  MusicViewController.swift
//  LabTunes
//
//  Created by Eric Martínez Paredes on 10/11/18.
//  Copyright © 2018 ACL. All rights reserved.
//

import UIKit

class MusicViewController: UIViewController {

    @IBOutlet weak var musicTableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    var songs: [Song] = []
    
    func downloadSongs() {
        Music.fetchSongs { (result: [Song]) in
            self.songs = result
            DispatchQueue.main.async {
                self.musicTableView.reloadData()
            }
        }
    }
    
    func downloadSongsBy (name: String) {
        Music.fetchSongs(songName: name) { (result: [Song]) in
            self.songs = result
            DispatchQueue.main.async {
                self.musicTableView.reloadData()
            }
        }
    }
    
    //Search Bar
    func setupSearchBar() {
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search Songs"
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
    //Fin Search Bar
    
    
    override func viewDidLoad() {
        // Do any additional setup after loading the view.n
        super.viewDidLoad()
        downloadSongs()
        setupSearchBar()
        
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MusicViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MusicCell", for: indexPath)
        cell.textLabel?.text = songs[indexPath.row].name
        return cell
    }
}

extension MusicViewController: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        downloadSongsBy(name: searchController.searchBar.text!)
    }
    
}
