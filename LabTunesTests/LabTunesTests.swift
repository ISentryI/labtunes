//
//  LabTunesTests.swift
//  LabTunesTests
//
//  Created by Eric Martínez Paredes on 09/11/18.
//  Copyright © 2018 ACL. All rights reserved.
//

import XCTest
@testable import LabTunes

class LabTunesTests: XCTestCase {

    override func setUp() {
        //Se correo al inicio de cada prueba
        // Put setup code here. This method is called before the invocation of each test method in the class.
        //let session = Session.sharedInstance
        //session.token = nil
        Session.sharedInstance.token = nil
        
        
        
    }

    override func tearDown() {
        // Cuando termina
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        
        
        
        
    }
    
    //Valida que la función devuelva un true
    func testCorrectLogin() {
        XCTAssertTrue(User.login(userName: "username", password: "password"))
    }
    
    func testWrongLogin(){
        XCTAssertFalse(User.login(userName: "username", password: "passwords"))
    }

    //Probar si la sesión se está almacenando correctamente al intentar hacer un login y verificar que se esté generando un token
    func testSaveSession(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "username", password: "password")
        XCTAssertNotNil(session.token)
    }

    func testLoginFailed(){
        let session = Session.sharedInstance
        let _ = User.login(userName: "username", password: "passwordsss")
        XCTAssertNil(session.token)
    }
    
    func testExpectedToken() {
        let _ = User.login(userName: "username", password: "password")
        let session = Session.sharedInstance
        XCTAssertEqual(session.token!, "1234567890", "Token should be equal")
    }
    
    func testNotExpectedToken() {
        let _ = User.login(userName: "username", password: "password")
        let session = Session.sharedInstance
        XCTAssertNotEqual(session.token!, "123456789", "Token isn't equal")
    }
    
    func testThrowsException() {
        let _ = User.login(userName: "username", password: "passwordsss")
        XCTAssertThrowsError(try User.fetchSongs())
    }
    
    
    func testMusicSongs() {
        var resultSongs: [Song] = []
        let promise = expectation(description: "Songs Downloaded")
        Music.fetchSongs { (songs) in
            resultSongs = songs
            promise.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
        XCTAssertNotEqual(resultSongs.count,0)
    }
    
}
